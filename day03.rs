#![feature(iter_array_chunks)]

fn main() {
	// part 1
	println!("{}",
		std::io::stdin().lines()
		.map(Result::unwrap).map(String::into_bytes)
		.map(|mut s| { let r = s.split_off(s.len() / 2); (s, r) })
		.map(|(a, b)| a.into_iter().find(|x| b.iter().any(|y| x == y)).unwrap())
		.map(|c| match c { b'a'..= b'z' => c - b'a', b'A'..=b'Z' => c - b'A' + 26, _ => 0 } as u32 + 1)
		.sum::<u32>()
	);
	// part 2
	println!("{}",
		std::io::stdin().lines()
		.map(Result::unwrap).map(String::into_bytes)
		.array_chunks::<3>()
		.map(|[a, b, c]| a.into_iter().find(|x| b.iter().any(|y| x == y) && c.iter().any(|z| x == z)).unwrap())
		.map(|c| match c { b'a'..= b'z' => c - b'a', b'A'..=b'Z' => c - b'A' + 26, _ => 0 } as u32 + 1)
		.sum::<u32>()
	);
}
