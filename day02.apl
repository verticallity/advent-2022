⍝ takes input as right argument of vector of letter pairs
⍝ for example, the sample input would be 'AY' 'BX' 'CZ'

⍝ part 1
+/(('ABC'⍳⊣)(⊢+3×3|1--)'XYZ'⍳⊢)/¨

⍝ part 2
+/(('ABC'⍳⊣)(¯2+(3|+)+3×⊢)'XYZ'⍳⊢)/¨
