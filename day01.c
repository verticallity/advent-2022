#include <stdio.h>
#include <stdlib.h>

// given reverse-sorted int array, insert a new element in order,
// and shift later elements right (discarding the last element)
void insert(int* arr, size_t count, int adding)
{
	int i = 0;
	while (i < count) {
		if (adding > arr[i]) {
			break;
		}
		++i;
	}
	while (i < count) {
		int swp = arr[i];
		arr[i] = adding;
		adding = swp;
		++i;
	}
}

int main()
{
	const int NUM_TOP = 3;
	int top[NUM_TOP];
	for (int i = 0; i <= NUM_TOP; ++i) {
		top[i] = 0;
	}
	int cur = 0;

	char buf[8];
	while (fgets(buf, 8, stdin)) {
		if (buf[0] == '\n') {
			insert(top, NUM_TOP, cur);
			cur = 0;
		} else {
			cur += atoi(buf);
		}
	}

	puts("\n-----\n");
	int total = 0;
	for (int i = 0; i < NUM_TOP; ++i) {
		printf("#%i: %i\n", i + 1, top[i]);
		total += top[i];
	}
	printf("total: %i\n", total);

	return 0;
}
