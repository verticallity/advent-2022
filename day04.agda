open import Agda.Primitive
  renaming (Set to Type ; Setω to Typeω)
  hiding (Prop)

data ⊥ : Type where

data 𝔹 : Type where
  true false : 𝔹

data ℕ : Type where
  zero : ℕ
  suc : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

_+_ : ℕ → ℕ → ℕ
zero + b = b
suc a + b = suc (a + b)

data List {ℓ} (A : Type ℓ) : Type ℓ where
  _::_ : A → List A → List A
  [] : List A

map : ∀ {α β} {A : Type α} {B : Type β} → (A → B) → List A → List B
map f [] = []
map f (x :: xs) = f x :: map f xs

fold : ∀ {α β} {A : Type α} {B : Type β} → (A → B → A) → A → List B → A
fold _⦉_ a0 [] = a0
fold _⦉_ a0 (x :: xs) = fold _⦉_ (a0 ⦉ x) xs

data Range {ℓ} (A : Type ℓ) : Type ℓ where
  _-_ : A → A → Range A

data Pair {ℓ} (A : Type ℓ) : Type ℓ where
  _,_ : A → A → Pair A

infixl 30 _-_
infixl 20 _,_
infixr 10 _::_


data _⊕_ {α β} (A : Type α) (B : Type β) : Type (α ⊔ β) where
  inl : A → A ⊕ B
  inr : B → A ⊕ B

record _⊗_ {α β} (A : Type α) (B : Type β) : Type (α ⊔ β) where
  constructor _,_
  field
    fst : A
    snd : B
open _⊗_

infixl 15 _⊗_
infixl 10 _⊕_

data Dec {ℓ} (A : Type ℓ) : Type ℓ where
  yes : A → Dec A
  no : (A → ⊥) → Dec A

¿_ : ∀ {ℓ} (A : Type ℓ) ⦃ _ : Dec A ⦄ → Dec A
¿_ _ ⦃ d ⦄ = d

infix 5 ¿_


data _≤_ : ℕ → ℕ → Type where
  0≤ : (a : ℕ) → 0 ≤ a
  suc≤suc : {a b : ℕ} → a ≤ b → suc a ≤ suc b

instance
  Dec≤ : {a b : ℕ} → Dec (a ≤ b)
  Dec≤ {0} {b} = yes (0≤ b)
  Dec≤ {suc a} {0} = no (λ {()})
  Dec≤ {suc a} {suc b} with Dec≤ {a} {b}
  … | yes p = yes (suc≤suc p)
  … | no ¬p = no (λ { (suc≤suc p) → ¬p p })

  Dec⊕ : ∀ {α β} {A : Type α} {B : Type β} ⦃ _ : Dec A ⦄ ⦃ _ : Dec B ⦄ → Dec (A ⊕ B)
  Dec⊕ ⦃ yes a ⦄ ⦃ _ ⦄ = yes (inl a)
  Dec⊕ ⦃ no ¬a ⦄ ⦃ yes b ⦄ = yes (inr b)
  Dec⊕ ⦃ no ¬a ⦄ ⦃ no ¬b ⦄ = no λ { (inl a) → ¬a a ; (inr b) → ¬b b }

  Dec⊗ : ∀ {α β} {A : Type α} {B : Type β} ⦃ _ : Dec A ⦄ ⦃ _ : Dec B ⦄ → Dec (A ⊗ B)
  Dec⊗ ⦃ yes a ⦄ ⦃ yes b ⦄ = yes (a , b)
  Dec⊗ ⦃ yes a ⦄ ⦃ no ¬b ⦄ = no λ (_ , b) → ¬b b
  Dec⊗ ⦃ no ¬a ⦄ ⦃ _ ⦄ = no λ (a , _) → ¬a a


FullyContains : Range ℕ → Range ℕ → Type
FullyContains (a - b) (x - y) = a ≤ x ⊗ y ≤ b

EitherFullyContains : Pair (Range ℕ) → Type
EitherFullyContains (r1 , r2) = FullyContains r1 r2 ⊕ FullyContains r2 r1

Overlap : Pair (Range ℕ) → Type
Overlap (a - b , x - y) = a ≤ x ⊗ x ≤ b ⊕ x ≤ a ⊗ a ≤ y

instance
  DecEitherFullyContains : {p : Pair (Range ℕ)} → Dec (EitherFullyContains p)
  DecEitherFullyContains {_ - _ , _ - _} = Dec⊕

  DecOverlap : {p : Pair (Range ℕ)} → Dec (Overlap p)
  DecOverlap {_ - _ , _ - _} = Dec⊕

decbool : ∀ {ℓ} {A : Type ℓ} → Dec A → 𝔹
decbool (yes _) = true
decbool (no _) = false

bool01 : 𝔹 → ℕ
bool01 false = 0
bool01 true = 1


-- input can be given inline in interactive mode with :: for end of line and [] for end of input
-- e.g. the sample input
-- 2 - 4 , 6 - 8 :: 2 - 3 , 4 - 5 :: 5 - 7 , 7 - 9 :: 2 - 8 , 3 - 7 :: 6 - 6 , 4 - 6 :: 2 - 6 , 4 - 8 :: []

part1 : List (Pair (Range ℕ)) → ℕ
part1 lines = fold _+_ 0 (map (λ l → bool01 (decbool (¿ EitherFullyContains l))) lines)

part2 : List (Pair (Range ℕ)) → ℕ
part2 lines = fold _+_ 0 (map (λ l → bool01 (decbool (¿ Overlap l))) lines)
